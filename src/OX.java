/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
import java.util.Scanner;
/**
 *
 * @author Acer
 */

public class OX {
    public static void main(String[] args) {
        Scanner in =new Scanner(System.in);
        System.out.println("Welcome to OX Game");
        
        //board
        char[][] board =new char[4][4];
        for(int i =1;i<4;i++){
            for(int j=1;j<4;j++){
                board[i][j] ='-';
            }
        }
        
        boolean player1=true;
        boolean end=false;
        
        while(!end){
            draw(board);
            
            if(player1){
                System.out.println("Turn O");
            }
            else{
                System.out.println("Turn X");
            }
            
            char c='-';
            if(player1){
                c='O';
            }
            else{
                c='X';
            }
            
            int row=0;
            int col=0;
           
            while(true){
                System.out.print("Please input row, col: ");
                row=in.nextInt();
                col=in.nextInt();
                
                if(row <1 || col<1 ||row >3||col>3){
                    System.out.println("This position is off the bounds of the board!");
                }
                else if(board[row][col] != '-'){
                    System.out.println("Someone is already in this position!");
                }
                else{
                    break;
                }
            }
            
            board[row][col] =c;
            draw(board);
            
            if(playerWon(board)=='O'){
                System.out.println(">>>O Win<<<");
                end=true;
            }
            else if(playerWon(board)=='X'){
                System.out.println(">>>X Win<<<");
                end=true;
            }
            else{
                if(boardDraw(board)){
                    System.out.println(">>> Draw <<<");
                    end=true;
                }
                else{
                    player1= !player1;
                }
            }
            
        }
        
        //draw(board);
        
        //System.out.print("Please input row, col: ");
       // int row=in.nextInt();
       // int col=in.nextInt();
    }
    public static void draw(char[][] board) {
        for(int i =1;i<4;i++){
            for(int j=1;j<4;j++){
                System.out.print(board[i][j]);
            }
            System.out.println();
        }
    }
    
    public static char playerWon(char[][] board){
        for(int i =1;i<4;i++){
            if(board[i][1] == board[i][2] && board[i][2] == board[i][3] && board[i][1] != '-'){
                return board [i][1];
            }
        }
        for(int j=1;j<4;j++){
            if(board[1][j] == board[2][j] && board[2][j] == board[3][j] && board[1][j] != '-'){
                return board [1][j];
            }
        }
        
        if(board[1][1]==board[2][2] && board[2][2]==board[3][3] && board[1][1]!='-' ){
            return board[1][1];
        }
        if(board[3][1]==board[2][2] && board[2][2]==board[1][3] && board[3][1]!='-' ){
            return board[3][1];
        }
        return ' ';
    }
    
    public static boolean boardDraw(char[][] board) {
        for(int i =1;i<4;i++){
            for(int j=1;j<4;j++){
                if(board[i][j] == '-'){
                    return false;
                }
            }
        }
        return true;
        
    }

    
    
    
}
